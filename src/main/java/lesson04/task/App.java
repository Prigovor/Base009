package lesson04.task;

/**
 * Created by Prigovor on 02.07.2017.
 */

public class App {
    public static void main(String[] args) {

        Figure figure = new Figure();
        figure.figureInfo();
        figure.area();
        figure.perimeter();

        Figure rectangle = new Rectangle(50, 70);
        rectangle.figureInfo();
        rectangle.area();
        rectangle.perimeter();
    }
}
