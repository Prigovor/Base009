package lesson04.task;

/**
 * Created by Prigovor on 02.07.2017.
 */

public class Figure {

    public void figureInfo() {
        System.out.println("The figure is not defined.");
    }

    public void area() {
        System.out.println("The figure is not defined.");
    }

    public void perimeter() {
        System.out.println("The figure is not defined.");
    }
}
