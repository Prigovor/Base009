package lesson04.task;

/**
 * Created by Prigovor on 02.07.2017.
 */

public class Rectangle extends Figure {

    private Integer sideA;
    private Integer sideB;
    private final Integer TWO = 2;

    public Rectangle(Integer sideA, Integer sideB) {
        this.sideA = sideA;
        this.sideB = sideB;
    }

    @Override
    public void figureInfo() {
        System.out.println("This is rectangle." + "\n" +
        "SideA = " + getSideA() + "\n" +
        "SideB = " + getSideB());
    }

    @Override
    public void area() {
        Integer area = getSideA() * getSideB();
        System.out.println("The area of the rectangle is: " +  area);
    }

    @Override
    public void perimeter() {
        Integer perimeter = TWO * (getSideA() + getSideB());
        System.out.println("The perimeter of the rectangle is: " + perimeter);
    }

    public Integer getSideA() {
        return sideA;
    }
    public void setSideA(Integer sideA) {
        this.sideA = sideA;
    }
    public Integer getSideB() {
        return sideB;
    }
    public void setSideB(Integer sideB) {
        this.sideB = sideB;
    }
}
