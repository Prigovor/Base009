package lesson04;

import lesson04.phone.Phone;
import lesson04.phone.SmartPhone;

/**
 * Created by Prigovor on 02.07.2017.
 */

public class Test {
    public static void main(String[] args) {
        ClassA classA = new ClassA("ClassA", 200);

        Phone phone = new Phone("LG");
        phone.about();

        SmartPhone smartPhone = new SmartPhone("Samsung" , "Android");
        smartPhone.about();
    }
}
