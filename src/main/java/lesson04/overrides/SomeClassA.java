package lesson04.overrides;

/**
 * Created by Prigovor on 02.07.2017.
 */

public class SomeClassA {
    public void method() {
        System.out.println("method() - SomeClassA");
    }
}

class SomeClassB extends SomeClassA {

    @Override
    public void method() {
        System.out.println("method() - SomeClassB");
    }
}

class SomeClassC extends SomeClassB {

}