package lesson04;

/**
 * Created by Prigovor on 02.07.2017.
 */

public class ClassA {

    private String strParam;
    private Integer intParam;

    public ClassA(String strParam, Integer intParam) {
        this.strParam = strParam;
        this.intParam = intParam;
    }
}

class ClassB extends ClassA {

    private Double dblParam;

    public ClassB(String strParam, Integer intParam, Double dblParam) {
        super(strParam, intParam);
        this.dblParam = dblParam;
    }
}