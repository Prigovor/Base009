package lesson04.phone;

/**
 * Created by Prigovor on 02.07.2017.
 */

public class Phone {

    private String model;
    private Integer year;

    public Phone() {
        this.model = "No Model";
    }

    public Phone(String model) {
        System.out.println("Constructor Phone is started");
        this.model = model;
        System.out.println("Constructor Phone is stop");
    }

    public Phone(String model, Integer year) {
        this.model = model;
        this.year = year;
    }

    public void about() {
        System.out.println(getModel());
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public Integer getYear() {
        return year;
    }
    public void setYear(Integer year) {
        this.year = year;
    }
}
