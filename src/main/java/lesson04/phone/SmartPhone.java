package lesson04.phone;

/**
 * Created by Prigovor on 02.07.2017.
 */

public class SmartPhone extends Phone {

    private String os;

    public SmartPhone(String model, String os) {
        super(model);
        this.os = os;
    }

    public SmartPhone(String model, Integer year) {
        setModel(model);
        setYear(year);
    }

    @Override
    public void about() {
        super.about();
        System.out.println(getOs());
    }

    public String getOs() {
        return os;
    }
    public void setOs(String os) {
        this.os = os;
    }
}
