package lesson03;

/**
 * Created by Prigovor on 01.07.2017.
 */

public class Car {
    public String model;
    public String color;
    public Integer year;

    public Car() {
        model = "KIA";
        color = "Red";
        year = 2000;
    }

    public Car(String color) {
        model = "KIA";
        this.color = color;
        year = 2000;
    }

    public Car(String color, Integer year) {
        model = "KIA";
        this.color = color;
        this.year = year;
    }

    public void about() {
        System.out.println(model + " " + color + " " + year);
    }
}