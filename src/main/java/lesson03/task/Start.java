package lesson03.task;

/**
 * Created by Prigovor on 01.07.2017.
 */

public class Start {
    public static void main(String[] args) {
        Phone phone = new Phone("Nokia", new SimCard("Vodaphone"));
        System.out.println(phone);
    }
}