package lesson03.task;

/**
 * Created by Prigovor on 01.07.2017.
 */

public class SimCard {

    private Long balance;
    private String operator;

    public SimCard() {
        this.balance = 0L;
    }

    public SimCard(String operator) {
        this.operator = operator;
    }

    public SimCard(Long balance, String operator) {
        this.balance = balance;
        this.operator = operator;
    }

    public Long getBalance() {
        return balance;
    }
    public void setBalance(Long balance) {
        this.balance = balance;
    }
    public String getOperator() {
        return operator;
    }
    public void setOperator(String operator) {
        this.operator = operator;
    }

    @Override
    public String toString() {
        return  " Balance: " + balance +
                " Operator: " + operator;
    }
}