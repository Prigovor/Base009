package lesson03.task;

import java.util.ArrayList;

/**
 * Created by Prigovor on 01.07.2017.
 */

public class Phone {

    private String model;
    private SimCard simCard;
    private ArrayList<Contact> contacts;

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        this.model = model;
    }
    public SimCard getSimCard() {
        return simCard;
    }
    public void setSimCard(SimCard simCard) {
        this.simCard = simCard;
    }
    public ArrayList<Contact> getContacts() {
        return contacts;
    }
    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public Phone(String model, SimCard simCard) {
        this.model = model;
        this.simCard = simCard;
        contacts = new ArrayList<>();
    }

    @Override
    public String toString() {
        return  "Model:   " + model + "\n" +
                "SimCard: " + simCard + "\n";
    }
}