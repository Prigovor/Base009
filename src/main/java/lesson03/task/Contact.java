package lesson03.task;

/**
 * Created by Prigovor on 01.07.2017.
 */

public class Contact {

    private String name;
    private Long phone;

    public Contact(String name, Long phone) {
        this.name = name;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Long getPhone() {
        return phone;
    }
    public void setPhone(Long phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return  "Name:  " + name + "\n" +
                "Phone: " + phone;
    }
}