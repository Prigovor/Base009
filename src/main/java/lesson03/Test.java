package lesson03;

/**
 * Created by Prigovor on 01.07.2017.
 */

public class Test {
    public static void main(String[] args) {
        Car car = new Car();
        car.about();
        Car car1 = new Car("White");
        car1.about();
    }
}
