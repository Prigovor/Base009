package HW02;

/**
 * Created by Prigovor on 02.07.2017.
 */

/**
 * Создаем клас который будет хранить числа(посредством полей) и выполнять
 * вычисления(с помощью методов).
 * Этот клас не содержит статический метод "main" и не запускает программу.
 * Он служит каркасом для создания нового ссылочного типа.
 * С его помощью мы создадим в исполняющем классе "App"  в методе "main"
 * ссылочную переменную типа NumberHW
 */
public class NumberHW {

    /**
     * Создаем поля класса(глобальные переменные)
     * firstNumb и secondNumb хранят в себе числа, над которыи будут
     * выполняться вычисления.
     * result хранит результат вычисления
     */
    private Integer firstNumb;
    private Integer secondNumb;
    private Integer result;

    /**
     * Создаем конструктор с двумя параметрами
     *
     * @param firstNumb
     * @param secondNumb
     * С попощью парамертов мы передаем(сохраняем) значения в одноименные
     * глобальные переменные, а чтобы избежать сокрытия пременных мы
     * используем указатель this который указывает на то что мы
     * иницыализируем имеено глобальные перемееные(поля класса)
     */
    NumberHW(Integer firstNumb, Integer secondNumb) {
        this.firstNumb = firstNumb;
        this.secondNumb = secondNumb;
    }

    /**
     * Создаем метод который будет выполнять операцию
     * сложения над переменными firstNumb и  secondNumb.
     * @param numberHW парамерт меода который прнимает ссылочную
     *                 пременную типа NumberHW.
     * @return метод возвращает ссылочную переменную типа NumberHW
     * с уже произведенным дейсвием сложения
     */
    public NumberHW add(NumberHW numberHW) {
        numberHW.setResult(numberHW.getFirstNumb() + numberHW.getSecondNumb());
        return numberHW;
    }

    /**
     * Создаем геттеры и сеттеры для полей класса чтобы обеспечить
     * к ним контролируемый доступ извне
     * */
    public Integer getFirstNumb() {
        return firstNumb;
    }
    public void setFirstNumb(Integer firstNumb) {
        this.firstNumb = firstNumb;
    }
    public Integer getSecondNumb() {
        return secondNumb;
    }
    public void setSecondNumb(Integer secondNumb) {
        this.secondNumb = secondNumb;
    }
    public Integer getResult() {
        return result;
    }
    public void setResult(Integer result) {
        this.result = result;
    }
}
