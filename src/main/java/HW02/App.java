package HW02;

/**
 * Created by Prigovor on 02.07.2017.
 */

public class App {

    /**
     * Метод main является точкой входа в программу с него
     * начинается работа нашего приложения
     */
    public static void main(String[] args) {
        /**
         * Создаем переменную типа NumberHW с именем numberHW
         * и передаем в параметры конструктора два произвольных
         * числа*/
        NumberHW numberHW = new NumberHW(10, 7);
        /*Обащаемся к переменной и вызываем метод add
        * а параметром передаем все ту же ссылочную перменную
        * экземпляра класса NumberHW*/
        numberHW.add(numberHW);
        System.out.println("Result: " + numberHW.getResult());

    }
}
