package test;

import lesson02.Visible;

/**
 * Created by Prigovor on 25.06.2017.
 */

public class TestClass extends Visible {
    public static void main(String[] args) {
        Visible.publicMethod();
        Visible.protectedMethod();
    }
}
