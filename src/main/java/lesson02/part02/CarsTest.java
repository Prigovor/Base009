package lesson02.part02;

/**
 * Created by Prigovor on 25.06.2017.
 */

public class CarsTest {

    public static void main(String[] args) {
        Car car1 = new Car();
        car1.model = "Ford";
        car1.color = "Black";
        car1.year = 2010;
        car1.start();
        car1.about();
        car1.stop();
        car1.about();

        Car car2 = new Car();
        car2.model = "Hundai";
        car2.color = "White";
        car2.year = 2012;
        car2.about();

        Car car3 = new Car();
        car3.about();
    }
}
