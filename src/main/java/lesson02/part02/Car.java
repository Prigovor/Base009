package lesson02.part02;

/**
 * Created by Prigovor on 25.06.2017.
 */

public class Car {

    public String model = "KIA";
    public String color = "Red";
    public Integer year = 2012;

    public Boolean stat = false;

    public void start() {
        stat = true;
    }

    public void stop() {
        stat = false;
    }

    public void about() {
        System.out.println("Model: " + model + "\n" +
                "Color: " + color + "\n" +
                "Year: " + year + "\n" +
                "Status: " + stat);
    }
}