package lesson02;

/**
 * Created by Prigovor on 25.06.2017.
 */

public class VisibleTest {

    public static void main(String[] args) {
        Visible.publicMethod();
        Visible.defaultMethod();
        Visible.protectedMethod();

        StaticExample.staticMethod();
        Integer anInt = StaticExample.INT;

        StaticExample st1 = new StaticExample();
        Double db = st1.dbl;
        st1.nonStaticMethod();

        StaticExample st2 = new StaticExample();
    }
}