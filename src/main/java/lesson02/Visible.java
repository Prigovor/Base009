package lesson02;

/**
 * Created by Prigovor on 25.06.2017.
 */

public class Visible {

    public static void publicMethod() {
        // TODO: 25.06.2017
    }

    protected static void protectedMethod() {
        // TODO: 25.06.2017
    }

    private static void privateMethod() {
        // TODO: 25.06.2017
    }

    static void defaultMethod() {
        // TODO: 25.06.2017
    }
}
