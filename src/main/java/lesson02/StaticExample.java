package lesson02;

/**
 * Created by Prigovor on 25.06.2017.
 */

public class StaticExample {

    public static final Integer INT = 1000;
    public Double dbl = 200.0;

    public static void staticMethod() {
        System.out.println("Static method");
    }

    public void nonStaticMethod() {
        System.out.println("Non Static method");
    }
}
