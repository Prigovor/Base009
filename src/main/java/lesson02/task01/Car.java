package lesson02.task01;

/**
 * Created by Prigovor on 25.06.2017.
 */

public class Car {

    private String model = "";
    private String color = "";
    private Integer year = 0;
    private Integer tank = 0;
    private Boolean status = false;
    private Driver driver = new Driver();
    ;

    public void go() {
        if (status) {
            System.out.println("Car going! >>>>>>>>>>>>>");
        } else {
            System.out.println("Car isn't started! Starting the car please.");
        }
    }

    public boolean start() {
        if (driver.getLicense()) {
            setStatus(true);
            System.out.println("Car is starting.");
            return true;
        } else {
            System.out.println("Driver haven't license!");
            setStatus(false);
            return false;
        }
    }

    public boolean charged(Integer gas) {
        if (gas > 0) {
            setTank(getTank() + gas);
            System.out.println("The tank is charged with: " + gas + " l.");
            return true;
        } else {
            System.out.println("Enter a number bigger than zero.");
            return false;
        }

    }

    public void driverSetLicense(Boolean isLicense) {
        getDriver().setLicense(isLicense);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Integer getTank() {
        return tank;
    }

    public void setTank(Integer tank) {
        this.tank = tank;
    }
}