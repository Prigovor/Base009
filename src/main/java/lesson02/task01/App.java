package lesson02.task01;


/**
 * Created by Prigovor on 25.06.2017.
 */

public class App {
    public static void main(String[] args) {

        Car car = new Car();
        car.driverSetLicense(true);
        car.start();
        car.go();
    }
}