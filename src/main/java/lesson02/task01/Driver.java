package lesson02.task01;

/**
 * Created by Prigovor on 25.06.2017.
 */

public class Driver {
    private String name;
    private Integer age;
    private Boolean license = false;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getLicense() {
        return license;
    }

    public void setLicense(Boolean license) {
        this.license = license;
    }
}
