package lesson01;

import java.util.Arrays;

/**
 * Created by Prigovor on 24.06.2017.
 */

public class Sort {

    public static int iteration = 0;

    public static void main(String[] args) {

        int[] base = randomArray(10);

        int[] test = copy(base);
        System.out.println(Arrays.toString(test));
        stupidSort(test);
        System.out.print(Arrays.toString(test));
        System.out.println(" " + iteration);

        test = copy(base);
        bubbleSort(test);
        System.out.print(Arrays.toString(test));
        System.out.println(" " + iteration);

        test = copy(base);
        selectionSort(test);
        System.out.print(Arrays.toString(test));
        System.out.println(" " + iteration);
    }

    public static int[] randomArray(int size) {
        int[] arr = new int[size];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 30);
        }
        return arr;
    }

    public static int[] copy(int[] oldArr) {
        int[] newArr = new int[oldArr.length];
        System.arraycopy(oldArr, 0, newArr, 0, oldArr.length);
        return newArr;
    }

    public static void stupidSort(int[] arr) {
        iteration = 0;
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] > arr[i + 1]) {
                int tmp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = tmp;
                i = -1;
            }
            iteration++;
        }
    }

    public static void bubbleSort(int[] arr) {
        iteration = 0;
        boolean swap = false;
        while (!swap) {
            for (int i = 0; i < arr.length - 1; i++) {
                if (arr[i] > arr[i + 1]) {
                    int tmp = arr[i];
                    arr[i] = arr[i + 1];
                    arr[i + 1] = tmp;
                    i = -1;
                    iteration++;
                }
            }
            swap = true;
            iteration++;
        }
    }

    public static void selectionSort(int[] arr) {
        iteration = 0;
        int minI = 0, minJ = 0;
        for (int i = 0; i < arr.length; i++) {
            minI = minJ = i;
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] < arr[minI] & arr[j] < arr[minJ]) {
                    minJ = j;
                }
                iteration++;
            }
            int tmp = arr[i];
            arr[i] = arr[minJ];
            arr[minJ] = tmp;
            iteration++;
        }
    }
}
